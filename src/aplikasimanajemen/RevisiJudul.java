package aplikasimanajemen;

public class RevisiJudul extends Judul {
    
    private String tgl_revisi, nim, judul_setelah;
    
    public RevisiJudul(String id_judul, String nim, String nama_judul, String judul_setelah, String tgl_revisi) {
        super(id_judul, nama_judul);
        this.nim = nim;
        this.judul_setelah = judul_setelah;
        this.tgl_revisi = tgl_revisi;
    }
    
    public RevisiJudul(String id_judul) {
        super(id_judul);
    }
    
    public RevisiJudul() {
        
    }
    
    public void setNim(String nim) {
        this.nim = nim;
    }
    
    public String getNim() {
        return nim;
    }
        
    public void setJudul(String judul_setelah) {
        this.judul_setelah = judul_setelah;
    }
    
    public String getJudul() {
        return judul_setelah;
    }
    
    public void setTglRevisi(String tgl_revisi) {
        this.tgl_revisi = tgl_revisi;
    }
    
    public String getTglRevisi() {
        return tgl_revisi;
    }
    
}
