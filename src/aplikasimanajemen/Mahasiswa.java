package aplikasimanajemen;

public class Mahasiswa {
    
    private String nim, namaMhs, prodi;
    private int jml_sks;
    
    public Mahasiswa(String nim, String namaMhs, String prodi, int jml_sks) {
        this.nim = nim;
        this.namaMhs = namaMhs;
        this.prodi = prodi;
        this.jml_sks = jml_sks;
    }
    
    public Mahasiswa(String nim) {
        this.nim = nim;
    }
    
    public Mahasiswa() {
        
    }
    
    public void setNim(String nim) {
        this.nim = nim;
    }
    
    public String getNim() {
        return nim;
    }
    
    public void setNamaMhs(String namaMhs) {
        this.namaMhs = namaMhs;
    }
    
    public String getNamaMhs() {
        return namaMhs;
    }
    
    public void setProdi(String prodi) {
        this.prodi = prodi;
    }
    
    public String getProdi() {
        return prodi;
    }
    
    public void setJmlSks(int jml_sks) {
        this.jml_sks = jml_sks;
    }
    
    public int getJmlSks() {
        return jml_sks;
    }
    
}
