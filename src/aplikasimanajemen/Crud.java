package aplikasimanajemen;

import javax.swing.JPanel;

public interface Crud {
    public void formatCrud(String sql, String jenis, 
            JPanel pnlInput, JPanel pnlButton, boolean bol);
    public void bersih(JPanel p);
}
