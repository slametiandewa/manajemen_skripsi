package aplikasimanajemen;

public class DosenPembimbing extends Civitas {
    private String sandos;
    
    public DosenPembimbing(String sandos, String namaCivitas, 
            String nidn, String email) {
        super(namaCivitas, nidn, email);
        this.sandos = sandos;
    }
    
    public DosenPembimbing(String sandos) {
        this.sandos = sandos;
    }
    
    public DosenPembimbing() {
        
    }
    
    public void setSandiDosen(String sandos) {
        this.sandos = sandos;
    }
    
    public String getSandiDosen() {
        return sandos;
    }
    
    

}
