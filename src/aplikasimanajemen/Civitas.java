package aplikasimanajemen;

public class Civitas {
    private String nidn, namaCivitas, email;
    
    public Civitas(String nidn, String namaCivitas, String email) {
        this.nidn = nidn;
        this.namaCivitas = namaCivitas;
        this.email = email;
    }
    
    public Civitas() {
        
    }
    
    public void setNamaCivitas(String namaCivitas) {
        this.namaCivitas = namaCivitas;
    }
    
    public String getNamaCivitas() {
        return namaCivitas;
    }
    
    public void setNidn(String nidn) {
        this.nidn = nidn;
    }
    
    public String getNidn() {
        return nidn;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getEmail() {
        return email;
    }
    
}
