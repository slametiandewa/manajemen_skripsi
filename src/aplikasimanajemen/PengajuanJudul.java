package aplikasimanajemen;

public class PengajuanJudul extends Judul {
     
    private String status, nim, tgl_submit;
    
    public PengajuanJudul(String id_judul, String nama_judul, String nim,
            String tgl_submit, String status) {
        super(id_judul, nama_judul);
        this.nim = nim;
        this.tgl_submit = tgl_submit;
        this.status = status;
    }
    
    public PengajuanJudul(String id_judul) {
        super(id_judul);
    }
    
    public PengajuanJudul() {
        
    }
    
    public void setNim(String nim) {
        this.nim = nim;
    }
    
    public String getNim() {
        return nim;
    }
    
    public void setTglSubmit(String tgl_submit) {
        this.tgl_submit = tgl_submit;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getStatus() {
        return status;
    }
    
}
