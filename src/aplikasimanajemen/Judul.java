package aplikasimanajemen;

public class Judul {
    
    private String nama_judul;
    private String id_judul;
    
    public Judul(String id_judul, String nama_judul) {
        this.id_judul = id_judul;
        this.nama_judul = nama_judul;
    }
    
    public Judul(String id_judul) {
        this.id_judul = id_judul;
    }
    
    public Judul() {
        
    }
    
    public void setIdJudul(String id_judul) {
        this.id_judul = id_judul;
    }
    
    public String getIdJudul() {
        return id_judul;
    }
    
    public void setJudul(String nama_judul) {
        this.nama_judul = nama_judul;
    }
    
    public String getJudul() {
        return nama_judul;
    }
}
