package aplikasimanajemen;

import java.awt.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TesCrud implements Crud {
    public Connection con = new koneksi().getCon();
    PreparedStatement stm = null;
    
    public void formatCrud(String sql, String jenis, JPanel pnlInput, JPanel pnlButton, boolean bol) {
        
        try {
            stm = con.prepareStatement(sql);
            stm.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Data berhasil di" + jenis, 
                    "Pesan", JOptionPane.INFORMATION_MESSAGE);
            bersih(pnlInput);
            enableAll(pnlInput, bol);
            enableAll(pnlButton, !bol);
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), 
            "Peringatan", JOptionPane.WARNING_MESSAGE);
        } finally {
            try {
//                stm.close();
//                con.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage(), 
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public void enableAll(JPanel p, boolean e) {
        for (Component c : p.getComponents()) {
            c.setEnabled(e);
        }
    }
    
    public void bersih(JPanel p) {
        for (Component c : p.getComponents()) {
            if(c instanceof JTextField) {
                JTextField txt = (JTextField) c;
                txt.setText("");
            } else if(c instanceof JComboBox) {
                JComboBox cbo = (JComboBox) c;
                cbo.setSelectedIndex(0);
            }
        }
    }
    
}
